import twint # Libreria para recolectar tweets
import threading # Libreria para usar hilos
import re # Libreria para usar expresiones regulares
from sentiment_analysis_spanish import sentiment_analysis # Libreria para el analisis de sentimientos
import pymysql # Libreria para conectarse a la base de datos

def buscarTweets(palabra, latitud,longitud,cantidad): # Funcion que busca los tweets con ayuda del API
    configuracion = twint.Config() # Confuguracion de tweet
    configuracion.Search = palabra # Buscar palabra
    configuracion.Geo=str(latitud)+","+str(longitud)+",1km"
    configuracion.Limit = cantidad # Limite de paquetes de tweets
    configuracion.Hide_output = True # Ocultar de la terminal los tweets recolectados
    configuracion.Since = "2020-02-15" # Tweets enviados desde la fecha
    # configuracion.Count = False # Contar los tweets
    configuracion.Store_object = True # Almacenar en un objeto json
    configuracion.Store_object_tweets_list = tweetsDescargados
    # Buscar twets
    twint.run.Search(configuracion)
    # return tweetsDescargados

def conexion_Mariadb():
    try:
        # Establecer la conexion
        conexion=pymysql.connect(
            host='localhost',
            user='cesar',
            password='password',
            database='tweets'
        )
    except mariadb.Error as e: # En cado de error en la conexion
        print("Error al conectarse a la base de datos",e)
    cursor=conexion.cursor() # Crear un cursor que se conecte a la base
    return cursor # Regresar cursor

def guardarTweets(tweet, inicio, fin):
    for i in range(inicio,fin): # Recorrer la lista desde el inicio y fin especificados
        db=conexion_Mariadb() # cursor que se conecta a la base de datos
        texto=limpiarTexto(tweet[i].tweet) # Limpiar texto del tweet
        analisis=analizar(texto) # Analisis de sentimientos
        query=("insert into `Tweets` values(null,%s,%s,%s,%s,%s)") # Query para insetar la información de los tweets en la base de datos
        try:
            db.execute(query,(tweet[i].id,texto,tweet[i].datestamp,sentimiento(analisis),idModulo)) # Ejecutar el query
        except (pymysql.err.IntegrityError,pymysql.err.DataError, pymysql.err.InternalError): # En caso de error al almacenar tweet
            print("El registro que intentas almacenar ya existe")
        db.close() # Terminar la conexión del cursor

def filtrarTweets(tweets):
    tweetsFiltrados=[]
    for tweet in tweets:
        db=conexion_Mariadb() # cursor que se conecta a la base de datos
        query=("select * from `Tweets` where `idTwitter` = %s") # Query para consultar si el id del tweet ya se encuentra en la base de datos
        consulta = db.execute(query,(tweet.id)) # Ejecutar Query
        db.close() # Terminar la conexión del cursor
        if consulta==0: # Compruba que no haya coincidencia
            tweetsFiltrados.append(tweet) # Agregar a la coleccion
    return tweetsFiltrados # Regresar la coleccion

def sentimiento(analisis):
    if analisis<=0.33: # Comprobar que el analisis es menor o igual que 0.33
        return 3 # Negativo
    elif analisis>0.33 and analisis<=0.67: # Comprobar que el analisis se encuentra en el rango de 0.33 y 0.67
        return 2 # Neutro
    else: # Comprobar que el analisis es mayor a 0.67
        return 1 # Positivo

def analizar(texto):
    sentiment = sentiment_analysis.SentimentAnalysisSpanish()
    return sentiment.sentiment(texto)

def limpiarTexto(texto):
    texto = texto.replace("#",'')
    texto = re.sub(r'https?:(/*[\w.]+)+', '', texto) #Quitar los links
    #Quitar mensiones
    texto = re.sub(r'[(]\s*@\s*(\w+\W*\s*)*[)]', '', texto)
    texto = re.sub(r'@\w*\W*', '', texto)
    texto = re.sub(r'[^\w*\s*]', '', texto) #Deja solo caracteres de palabra
    return texto

if __name__ == '__main__':
    numTweet = 20 # Numero de tweets que se descargaran
    db=conexion_Mariadb() # cursor que se conecta a la base de datos
    db.execute("select idModulo,nombre,latitud,longitud from `Modulo`") # Consultar los modulos de vacunacion en la base de datos
    db.close() # Cerrar la conexion a la base de datos
    palabras=["vacuna", "vacunado", "vacunandome", "AstraZeneca", "Pfizer", "Sputnik" ]# Palabras a buscar
    # Recolectar tweets para cada modulo
    for (idModulo,nombre,latitud,longitud) in db:
        tweetsDescargados=[] # Inicializar la lista para los tweets descargados
        tweetsFiltrados=[] # Inicializar la lista de los tweets filtrados
        print("*********************Modulo %s: %s*************************" %(idModulo, nombre))
        print("%%%%%%%%%%%%%%%%%%%%%%% Buscando tweets %%%%%%%%%%%%%%%%%%%%")
        # Para cada palabra buscar tweets
        for palabra in palabras:
            try:
                # tweetsDescargados = buscarTweets(palabra,latitud,longitud,numTweet) #Buscar tweets que coincidan con la palabra especificada
                buscarTweets(palabra,latitud,longitud,numTweet) #Buscar tweets que coincidan con la palabra especificada
            except (twint.token.RefreshTokenException):
                print("Error al buscar tweets con la palabra %s" %(palabra))
        print("--------------------- Verificando %s Tweets -----------------" %len(tweetsDescargados))
        tweetsFiltrados= filtrarTweets(tweetsDescargados) # Filtrar los tweets que aun no se almacenan
        print("--------------------- Comenzando con el guardado de %s tweets -----------------" %len(tweetsFiltrados))
        longitud=len(tweetsFiltrados) # Cantidad de tweets despues de filtrarlos
        # Hilos que se encargan de guardar tweets
        t1=threading.Thread(target=guardarTweets, args=(tweetsFiltrados,0,int(longitud/4),))
        t2=threading.Thread(target=guardarTweets, args=(tweetsFiltrados,int(longitud/4),int(longitud/2),))
        t3=threading.Thread(target=guardarTweets, args=(tweetsFiltrados,int(longitud/2),int(3*longitud/4),))
        t4=threading.Thread(target=guardarTweets, args=(tweetsFiltrados,int(3*longitud/4),longitud,))
        # Iniciar hilos
        t1.start()
        t2.start()
        t3.start()
        t4.start()
        # Esperar a que terminen los hilos
        t1.join()
        t2.join()
        t3.join()
        t4.join()
